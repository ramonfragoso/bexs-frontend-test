
import Checkout from './pages/Checkout';
import Navbar from './components/Navbar';

function App() {
  return (
    <div style={{height: '100%'}}>
      <Navbar/>
      <Checkout/>
    </div>
  );
}


export default App;

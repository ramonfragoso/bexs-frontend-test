import { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '80%',
        paddingRight: '12px'
    },
    content: {
        background: 'linear-gradient(90deg, #DE4B4B 35%, white 35%)',
        height: '500px',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: '30px',
        // border: 'solid'
    }
}))

function Content() {
    
    const classes = useStyles();
    const [cardNumber, setCardNumber] = useState();
    const [name, setName] = useState();
    const [valid, setValid] = useState();
    const [CVV, setCVV] = useState();
    const [operation, setOperation] = useState();


    return (
        <div className={classes.root}>
            <div className={classes.content}>
                <div>cartao</div>
                <div>Form</div>
            </div>
        </div>
    )
}

export default Content;
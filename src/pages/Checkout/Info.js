import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    root: {
        background: 'white',
        width: '20%',
        paddingLeft: '12px',
        height: '200px'
    },
    content: {
        background: 'white',
    }
}))

function Info() {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <div className={classes.content}>
                CONTENT
            </div>
        </div>
    )
}

export default Info;
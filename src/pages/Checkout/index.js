import { Hidden } from '@material-ui/core';
import Container from '@material-ui/core/Container';
import { makeStyles } from '@material-ui/core/styles';
import Content from './Content'
import Info from './Info';

const useStyles = makeStyles((theme) => ({
    container: {
        height: '80%',
        padding: 0,
    },
    wrapper: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        height: '100%',
        width: '100%',
    },
    layout: {
        display: 'flex',
        flexDirection: 'row',
        width: '100%',
    }
}))

function Checkout() {
    const classes = useStyles();

    return (
        <Container className={classes.container}>
            <div className={classes.wrapper}>
                <div className={classes.layout}>
                    <Content/>
                    <Hidden smDown>
                        <Info/>
                    </Hidden>
                </div>
            </div>
        </Container>
    )
}

export default Checkout;
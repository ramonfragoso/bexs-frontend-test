import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import Hidden from '@material-ui/core/Hidden';
import {ReactComponent as Logo} from '../../assets/logo.svg';

const useStyles = makeStyles((theme) => ({
    root: {
        boxShadow: 'none',
        color: 'black',

    },
    navbar: {
        background: 'white',
        borderBottom: '1px solid #E6E6E6'
    },
    container: {   
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        height: '100px',
    },
    logo: {
        width: '300px'
    },
    buttonGroup: {
        display: 'flex',
        flexDirection: 'row'
    },
    button: {
        height: '12px',
        width: '70px',
        background: '#9C9C9C',
        borderRadius: '5px',
        margin: '0px 10px'
    }
}));

export default function Navbar() {
  const classes = useStyles();

  return (
      <Hidden smDown>
        <AppBar className={classes.root}  position="sticky">
            <Toolbar className={classes.navbar} >
                <Container className={classes.container} >
                    <div>
                        <Logo/>
                    </div>
                    <div className={classes.buttonGroup}>
                        <Box className={classes.button}/>
                        <Box className={classes.button}/>
                        <Box className={classes.button}/>
                        <Box className={classes.button}/>
                        <Box className={classes.button}/>
                    </div>
                </Container>
            </Toolbar>
        </AppBar>
      </Hidden>
  );
}